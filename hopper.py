#!/usr/bin/python

import sys
import requests
import hmac
import hashlib
import urllib.parse
import time
import datetime
import json

from simple_rest_client.api import API

class PoolAPI:
    def __init__(self, url):
        self._url = url
        self.api = API(
            api_root_url=url,
            params={},
            headers={},
            timeout=4,
            append_slash=False,
            json_encode_body=True,
        )
        self.api.add_resource(resource_name='stats')

    def get_stats(self):
        r = self.api.stats.list(body=None, params={}, headers={})
        v = int(r.body['stats']['roundShares']) / int(r.body['nodes'][0]['difficulty']) * 100
        h = int(r.body['hashrate']) / 1000000000
        return v, h

def parse_epools(epools_tpl):
    for prop in epools_tpl.split(','):
        if prop.startswith(' API:'):
            return prop[5:]

logs = []
def log(message):
    print(message)
    logs.append(message)

def run():
    import configparser
    config = configparser.ConfigParser()
    config.read('/hive-config/hopper.conf')

    login = config["API"]["LOGIN"]
    password = config["API"]["PASSWORD"]

    farm_id =  int(config["API"]["FARM_ID"])
    worker_id = int(config["API"]["RIG_ID"])

    url = 'https://api2.hiveos.farm/api/v2'
    url_login = url + '/auth/login'
    url_farms = url + '/farms/{}'.format(farm_id)
    url_workers = url_farms + '/workers'
    url_worker = url_workers + '/' + str(worker_id)
    url_fs = url_farms + '/fs'

    json = {'login':login, 'password': password}
    response = requests.post(url_login, json=json)

    headers = {"Authorization":'Bearer ' + response.json()['access_token']}

    response = requests.get(url_fs, headers=headers)
    wallets = response.json()['data']

    response = requests.get(url_worker, headers=headers)
    current_pool_id_1 = response.json()['flight_sheet']['id']

    best_pool_var = 500000
    best_pool_id = 0

    log("Current 1: {}".format(current_pool_id_1))
    for wallet in wallets:
        api = parse_epools(wallet['items'][0]['miner_config']['epools_tpl']).strip()
        if api == 'default':
            detault_pool_id = wallet['id']
        else:
            v, h = PoolAPI(api).get_stats()
            log("{}-{} Var: {:.2f} GHS: {:.2f}".format(wallet['id'] , api.ljust(50) , v, h))
            if v < best_pool_var and h > 3:
                best_pool_var = v
                best_pool_id = wallet['id']

    if best_pool_var > 100:
        log("Set defaul as best")
        best_pool_id = detault_pool_id

    log("Best: {}".format(best_pool_id))
    if current_pool_id_1 != best_pool_id:
        log("Change to best")

        json = {"worker_ids": [worker_id], "data": {"fs_id": best_pool_id}}
        response = requests.patch(url_workers, json=json, headers=headers)
        log(str(response))
        return True
    else:
        return False
    

while True:
    print("".ljust(50, '-'))
    log(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    change = False
    try:
        change = run()
    except Exception as e:
        log(str(e))

    with open("/var/log/hopper.log", "a") as text_file:
        for l in logs:
            text_file.write(l)
            text_file.write('\n')


    with open("/var/log/hopper_last.log", "w") as text_file:
        for l in logs:
            text_file.write(l)
            text_file.write('\n')

    if change:
        with open("/var/log/hopper_changes.log", "a") as text_file:
            for l in logs:
                text_file.write(l)
                text_file.write('\n')

    logs = []
    time.sleep(60)
